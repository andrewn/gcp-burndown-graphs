"use strict";

var Promise = require("bluebird");
var google = require("googleapis");
var googleAuth = require("google-auth-library");

function getAuth() {
  let credentials = {
    installed: {
      client_id: process.env.GOOGLE_API_CLIENT_ID,
      project_id: process.env.GOOGLE_API_PROJECT_ID,
      auth_uri: "https://accounts.google.com/o/oauth2/auth",
      token_uri: "https://accounts.google.com/o/oauth2/token",
      auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
      client_secret: process.env.GOOGLE_API_CLIENT_SECRET,
      redirect_uris: ["urn:ietf:wg:oauth:2.0:oob", "http://localhost"]
    }
  };

  let clientCreds = {
    access_token: process.env.GOOGLE_CREDS_ACCESS_TOKEN,
    refresh_token: process.env.GOOGLE_CREDS_REFRESH_TOKEN,
    token_type: "Bearer",
    expiry_date: parseInt(process.env.GOOGLE_CREDS_EXPIRY_DATE, 10)
  };

  let clientSecret = credentials.installed.client_secret;
  let clientId = credentials.installed.client_id;
  let redirectUrl = credentials.installed.redirect_uris[0];
  let auth = new googleAuth();

  let oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
  oauth2Client.credentials = clientCreds;
  return oauth2Client;
}

function appendSheet(spreadsheetId, range, values) {
  return new Promise((resolve, reject) => {
    let sheets = google.sheets("v4");

    let request = {
      spreadsheetId: spreadsheetId,
      range: range,
      valueInputOption: "USER_ENTERED",
      insertDataOption: "OVERWRITE",
      resource: {
        values: [values]
      },
      auth: getAuth()
    };

    console.log("Appending values to spreadsheet");
    sheets.spreadsheets.values.append(request, function(err, response) {
      if (err) {
        return reject(err);
      }

      console.log("Successfully appended values to spreadsheet");
      return resolve(response);
    });
  });
}

module.exports = appendSheet;
